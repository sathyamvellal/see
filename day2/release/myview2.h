/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 08-01-2012
 */

#ifndef MYVIEW2_H
#define MYVIEW2_H
#include "view.h"

//MyView1 inherits from View
class MyView2 : public View
{
public:
	//displays the view
	virtual void disp();
	
	//Denotes that the view must be updated. Is called when the
	//	underlying model changes
	void update();
private:
	int cu;
};

#endif
