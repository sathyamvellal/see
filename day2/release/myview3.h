/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 08-01-2012
 */

#ifndef MYVIEW3_H
#define MYVIEW3_H
#include "view.h"

//MyView3 inherits from View
class MyView3 : public View
{
public:
	//displays the view
	virtual void disp();
	
	//Denotes that the view must be updated. Is called when the
	//	underlying model changes
	void update();
private:
	std::string sqRoot;
};

#endif
