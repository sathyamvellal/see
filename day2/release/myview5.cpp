/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 08-01-2012
 */

#include <iostream>
#include <string>
#include <sstream>
#include "myview5.h"
#include "mymodel.h"

void MyView5::update()
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	int t = p->getdata();
	compute(t);
}

void MyView5::disp()
{
	std::cout << "Pascal's triangle: \n" << pasTri << std::endl;
}

void MyView5::compute(int n)
{
	int i, j, k;
	int ele=1;
	std::stringstream s;
	pasTri = "";
	if (n>=0)
	{
		for (i=1; i<=n; i++)
        		pasTri += " ";
		s << ele;
    		pasTri += s.str() + "\n";
 		s.str(std::string());
		
	    	for (i=1; i<=n; i++)
	    	{   
			ele = 1;
		        for(k=1; k<=n-i; k++)
				pasTri += " ";
		//	std::stringstream s;
			s << ele;
			pasTri += s.str() + " ";
 		       	s.str(std::string());
			for (j=0; j<=i-1; j++)
 		       	{   
				ele= ele*(i-j)/(j+1);
		//		std::stringstream s;
        			s << ele;
				pasTri += s.str() + " ";
 		       		s.str(std::string());
        		}
			pasTri += "\n";
    		}
	}
}
