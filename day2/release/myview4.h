/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 08-01-2012
 */

#ifndef MYVIEW4_H
#define MYVIEW4_H
//#include <iostream>
#include "view.h"

//MyView1 inherits from View
class MyView4 : public View
{
public:
	//displays the view
	virtual void disp();
	
	//Denotes that the view must be updated. Is called when the
	//	underlying model changes
	void update();
private:
	//factors in string format
	std::string factors;
};

#endif
