/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 08-01-2012
 */

#include <iostream>
#include <sstream>
#include <cmath>
#include "myview3.h"
#include "mymodel.h"

void MyView3::update()
{
	std::stringstream ss;
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	int t = p->getdata();
	if (t >= 0)
	{
		ss << sqrt(t);
		sqRoot = ss.str();
	}
	else
	{
		ss << sqrt(-t);
		sqRoot = ss.str() + "i";
	}
}

void MyView3::disp()
{
	std::cout << "Square Root : " << sqRoot << std::endl;
}
