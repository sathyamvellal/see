/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2012
 */

#ifndef MYMODEL_H
#define MYMODEL_H
#include "model.h"

//inherits Model
class MyModel : public Model
{
public:
	//constructor with default value of 'val' as 0
	MyModel (int val = 0);
	
	//changes 'val'
	virtual void change();
	
	//returns 'val'
	virtual int getdata();

private:
	int val;
};

#endif
