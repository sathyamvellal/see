/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2012
 */

#include <iostream>
#include "model.h"
#include "mymodel.h"

MyModel::MyModel(int val): Model(), val(val)
{
}


int MyModel::getdata()
{
	return val;
}


void MyModel::change()
{
	try
	{
		std::cout << "Enter new value : ";
		std::cin >> val;
		Model::change();	//can throw "no controller is attached" exception
	}
	catch(const char *s)
	{
		std::cout << s << std::endl;
	}
}

