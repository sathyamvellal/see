/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2012
 */

#ifndef VIEW_H
#define VIEW_H
#include "controller.h"

//forward declaration
class Controller;


class View
{
public:
	//constructor. initializes the Controller to null
	View() : pController(0) {}
	
	//destructor
	virtual ~View() {};
	
	//binds the view to a controller
	//can throw an exception if a controller is already attached
	virtual void attach(Controller *);

	//detaches the view from the controller
	virtual void detach();
	
	//displays the view. Can be called when required.
	virtual void disp() = 0;
	
	//updates the view. Is called when there is a change to the underlying model
	virtual void update() = 0;

protected:
	//Pointer to the controller object to which the view is bound
	Controller *pController;
};

#endif
