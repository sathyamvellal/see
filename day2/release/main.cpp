/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2012
 * Last Modifed : 08-01-2012
 */

#include <iostream>
#include "mymodel.h"
#include "mycontroller.h"
#include "myview1.h"
#include "myview2.h"
#include "myview3.h"
#include "myview4.h"
#include "myview5.h"

int main()
{
	MyModel m(5);
	
	MyController c(&m);

	MyView1 v1;
	v1.attach(&c);
	MyView2 v2;
	v2.attach(&c);
	MyView3 v3;
	v3.attach(&c);
	MyView4 v4;
	v4.attach(&c);
	MyView5 v5;
	v5.attach(&c);

	v1.disp();
	v2.disp();
	v3.disp();
	v4.disp();
	v5.disp();

	m.change();

	v1.disp();
	v2.disp();
	v3.disp();
	v4.disp();
	v5.disp();

	v1.detach();
	v2.detach();
	v3.detach();
	v4.detach();
	v5.detach();
	
	return 0;
}
