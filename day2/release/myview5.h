/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 08-01-2012
 */

#ifndef MYVIEW5_H
#define MYVIEW5_H
#include <iostream>
#include "view.h"

//MyView1 inherits from View
class MyView5 : public View
{
public:
	//displays the view
	virtual void disp();
	
	//Denotes that the view must be updated. Is called when the
	//	underlying model changes
	void update();
private:
	//stores the pascal's triangle in the form of a string
	std::string pasTri;
	
	//function to generate pascal's triangle
	void compute(int n);
};

#endif
