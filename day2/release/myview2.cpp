/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 08-01-2012
 */

#include <iostream>
#include "myview2.h"
#include "mymodel.h"

void MyView2::update()
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	int t = p->getdata();
	cu = t * t * t;
}

void MyView2::disp()
{
	std::cout << "Cube : " << cu << std::endl;
}
