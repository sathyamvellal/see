/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 08-01-2012
 */

#include <iostream>
#include <sstream>
#include <string>
#include "myview4.h"
#include "mymodel.h"

void MyView4::update()
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	int t = p->getdata();

	if (t != 0)
	{
		if (t < 0)
			t = -t;
		factors = "1";
		std::stringstream ss;
		for (int i = 2; i <= t; ++i)
		{
			if (t % i == 0)
			{
				ss << i; //place integer in buffer
				factors += ", " + ss.str();
				ss.str(std::string()); //reset buffer
			}
		}
	}
	else
		factors = "0";
}

void MyView4::disp()
{
	std::cout << "Positive Factors : " << factors << std::endl;
}
