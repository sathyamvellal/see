/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2012
 */

#include "mycontroller.h"

MyController::MyController(Model *pModel) : Controller(pModel)
{
	pModel->setController(this);
}
