day 2:
Trust you enjoyed day1.

We shall add more views. We had a view to square the value in the model.
Add the following views.
a) cube view
b) square root view
c) all factors view
d) Pascal triangle view (take n % 10 as the input)

Please follow the steps.
1. make a new directory and copy all your files.
2. design the interface to care of the following views. suitably modify your 
   header files.
3. pass these header files to the team which should test your code.
4. receive the header files of the team whose code you should test.
5. write the test cases for these.
6. implement your design
7. pass your code for testing.
8. test your friend team's code and report to them
9. receive the test report and correct bugs if any.
10. pass your code for code review.
11. recieve the code and do the code review.

Provide  mechanisms to attach and detach views dynamically.
Whenever a view is displayed, it should depict the current model.


You may want to change the way controller remembers attached views.
You may follow a time schedule.
- make your design : 8.15am to 9.45am
- develop test cases for the other team : 9.45 am to 10.15 am
- Do take a break!
- Develop your soln : 10.45 am - 12.45 pm
  divide the work so that member1 develops, member2 watches. Do not develop
  in parallel.
- Test round(s) : 1.30 pm - 2.30 pm
- Code review : 2.30 pm - 3.30 pm

