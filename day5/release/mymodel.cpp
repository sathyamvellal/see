/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2013
 * Last Modified : 09-01-2013
 */

#include <iostream>
#include "model.h"
#include "mymodel.h"
#include "node.h"
#include "list.h"


void MyModel::change()
{
	try
	{
		Model::change();	//can throw "no controller is attached" exception
	}
	catch(const char *s)
	{
		std::cout << s << std::endl;
	}
}

void MyModel::addEnd(int num) 
{
	d.addEnd(num);
	change();
}

int MyModel::removeEnd()	
{
	int v = d.removeEnd();
	change();
	return v;
}

void MyModel::addBeg(int num) 
{
	d.addBeg(num);
	change();
}


int MyModel::removeBeg() 
{
	int v = d.removeBeg();
	change();
	return v;
}

const Node& MyModel::getHead()
{
	return *(d.getHead());
}

const Node& MyModel::getTail()
{
	return *(d.getTail());
}

