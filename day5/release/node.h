/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 09-01-2013
 */


#ifndef NODE_H
#define NODE_H

class Node
{
	friend class DList;
private:
	int val;
	Node *next;
	Node *prev;
public:
	Node* getNext() {return next;}
	Node* getPrev() {return prev;}
	int getVal() {return val;}
};

#endif
