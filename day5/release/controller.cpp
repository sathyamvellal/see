/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2012
 * Last Modifed : 08-01-2013
 */

#include <iostream>
#include "controller.h"

Controller::Controller(Model *pModel): pModel(pModel), nextAvailable(0)
{
	for (int i = 0; i < MAX; ++i)
		pViews[i] = 0;
}

/*
 * Modified : 08-01-2013
 * 	- Found Bug : Infinite loop in the for-loop when the number views exceeds max.
 * 	  Status : Fixed
 */

void Controller::attach(View *pView)
{
	int i, j;
	if (nextAvailable == -1)
		throw "Failed to attach view!";
	pViews[nextAvailable] = pView;

	for (j = 0, i = nextAvailable; j < MAX && pViews[i] != 0; i = (i + 1) % MAX, ++j);
	nextAvailable = j < MAX ? i : -1;
}

void Controller::detach(View *pView)
{
	int i;
	for (i = 0; i < MAX && pViews[i] != pView; ++i);
	if (i < MAX)
	{
		pViews[i] = 0;
	}
}

Model* Controller::getModel()
{
	return pModel;
}

void Controller::notify()
{
	for(int i = 0; i < MAX; ++i)
	{
		if (pViews[i] != 0)
		{
			pViews[i]->update();
		}
	}
}

