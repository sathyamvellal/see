/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 09-01-2013
 */

#include "node.h"
#include "list.h"

DList::~DList()
{
	Node* temp = head;
	Node* old;

	while (temp != 0)
	{
		old = temp;
		temp = temp->next;
		delete old;
	}
}

DList::DList(const DList &d)
{
	Node* temp = d.head;

	while (temp != 0)
	{
		addEnd(temp->val);
		temp = temp->next;
	}
}
	
DList& DList::operator=(const DList &d)
{
	Node* temp = d.head;

	while (temp != 0)
	{
		addEnd(temp->val);
		temp = temp->next;
	}

	return *this;
}

void DList::addEnd(int num)
{
	Node *temp = new Node();
	
	temp->val = num;
	temp->next = 0;
	temp->prev = 0;
	
	if (head == 0)
	{
		head = temp;
		tail = temp;
	}
	else
	{
		temp->prev = tail;	
		tail->next = temp;
		tail = temp;
	}
}

int DList::removeEnd()
{
	Node *temp = tail;

	if (!temp)
		throw "List is empty!";

	int val = temp->val;
	
	if (head == tail)
	{
		head = 0;
		tail = 0;
	}
		else
	{
		tail = tail->prev;
		tail->next = 0;
	}

	delete temp;
	return val;
}

void DList::addBeg(int num)
{
	Node *temp = new Node();
	
	temp->val = num;
	temp->next = 0;
	temp->prev = 0;
	
	if (head==0)
	{
		head = temp;
		tail = temp;
	}
	else
	{	
		head->prev = temp;
		temp->next = head;
		head = temp;
	}
}

int DList::removeBeg()
{
	Node *temp = head;

	if (!temp)
		throw "List is empty!";

	int val = temp->val;
		if (head == tail)
	{
		head = 0;
		tail = 0;
	}
	else
	{
		head = head->next;
		head->prev = 0;
	}
		delete temp;
		
	return val;
}

Node* DList::getHead()
{
	return head;
}

Node* DList::getTail()
{
	return tail;
}
