/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 09-01-2013
 */

#ifndef LIST_H
#define LIST_H

class Node;

const int MAX = 10;
class DList
{
private:
	Node list[MAX];
	Node *head;
	Node *tail;
	
public:
	DList(): head(0), tail(0) {}
	~DList();
	DList(const DList &d);
	DList& operator=(const DList &d);
	
	void addEnd(int num);
	int removeEnd();
	void addBeg(int num);
	int removeBeg();
	
	Node* getHead();
	Node* getTail();
};

#endif
