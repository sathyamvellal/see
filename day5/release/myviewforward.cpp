/**
 * @authors Sathyam Vellal, Sidhi Adkoli
 * Created on: 09-01-2013
 */

#include <iostream>
#include "myviewforward.h"
#include "node.h"
#include "mymodel.h"

void MyViewForward::disp()
{
	std::cout << "Forward traversal: \n";
	const Node& temp = cur;
	if (temp)
	{
		while (temp)
		{
			std::cout << temp->getVal() << " -> ";
			temp = temp->getNext();
		}
		std::cout << "\b\b\b    \n";
	}
	else
	{
		std::cout << "NULL\n";
	}
}

void MyViewForward::update()
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	cur = p->getHead();
}
