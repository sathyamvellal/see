/**
 * @authors Sathyam Vellal, Sidhi Adkoli
 * Created on: 09-01-13
 */


#ifndef MYVIEWFORWARD_H
#define MYVIEWFORWARD_H
#include "view.h"

class Node;

//MyViewForward inherits from View
class MyViewForward : public View
{
public:
	//displays the view
	virtual void disp();
	
	//Denotes that the view must be updated. Is called when the
	//	underlying model changes
	void update();
	
//private:
	//used for traversing
	const Node& cur;
};

#endif

