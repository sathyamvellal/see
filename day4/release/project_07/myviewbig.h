#ifndef MYVIEWBIG_H
#define MYVIEWBIG_H
#include "view.h"

class MyViewBig : public View
{
public:
	virtual void biggest() const;
};

#endif
