#ifndef MYVIEW_H
#define MYVIEW_H
#include"view.h"

// square of number
class MyView : public View
{
public:
	virtual void disp_forward() const;
	virtual void disp_reverse() const;
};

#endif
