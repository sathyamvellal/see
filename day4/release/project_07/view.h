#ifndef VIEW_H
#define VIEW_H
#include"controller.h"

class Controller;

// View : definition
class View
{
public:
	View() : pController(0) {}
	virtual ~View(){};
	virtual void attach(Controller *);

protected:
	Controller *pController;
};

#endif
