#ifndef CONTROLLER_H
#define CONTROLLER_H

#include"model.h"
#include"view.h"

class Model;
class View;

// Controller : Definition

class Controller
{
public:
	Controller(Model *pModel = 0) : pModel(pModel), n(0) {}
	virtual void attach(View *pView);
	virtual void detach(View *pView);
	virtual Model* getModel();
	virtual ~Controller() { }
protected:
	Model *pModel;
	View  *pViews[10];
	int n;


};

#endif
