#include <iostream>
#include <cmath>
#include"mymodel.h"

void List::add_at_beginning(int n)
{
    Node *temp ;
    temp = new Node(n);
    temp->lptr = NULL;
    temp->rptr = NULL;

    if(head == NULL)
    {
        head = temp;
        tail = temp;
    }
    else
    {
        temp->rptr = head;
        head->lptr = temp;
        head = temp;
    }
}

void List::add_at_end(int n)
{
    Node *temp ;
    temp = new Node(n);
    temp->lptr = NULL;
    temp->rptr = NULL;
    
    if(head == NULL)
    {
        head = temp;
        tail = temp;
    }
    else
    {
    	temp->lptr = tail;
    	tail->rptr = temp;
    	tail = temp;
    }
}

void List::remove_at_beginninig()
{
	if(head && tail)
	{
		Node *temp = head;
		head = head->rptr;
		delete temp;
	}
	else
	{
		cout << "\nNo data to remove!!\n" << endl;
	}
}

void List::remove_at_end()
{
	if(tail && head)
	{
		Node *temp = tail;
		tail = tail->lptr;
		delete temp;
	}
	else
	{
		cout << "\nNo data to remove!!\n" << endl;
	}
}

void List::disp_forward()
{
    if(head)
    {
		Node *temp = head;
		cout << "\nDisplaying in forward order :" << endl;
		while(temp)
		{
		    cout << temp->get_key()<<" ";
		    temp = temp ->rptr;
		}
    }
    else
    {
    	cout << "NO DATA!!" << endl;
    }
    cout << endl;
}

void List::disp_reverse()
{
    if(head)
    {
		Node *temp = tail;
		cout << "\nDisplaying in reverse order :" << endl;
		while(temp)
		{
		    cout << temp->get_key()<<" ";
		    temp = temp ->lptr;
		}
	}
	else
    {
    	cout << "NO DATA!!" << endl;
    }
    cout << endl;
}

/*
 * Modified on : 10-01-2013
 * By : Sathyam M Vellal, Sidhi Adkoli
 */

int List::get_biggest() const
{
    Node* temp = head;
    Node* big;

    if (temp)
    {
        big = temp;
        temp = temp->rptr;

        while (temp)
        {
            if (temp->get_key() > big->get_key())
                big = temp;
            temp = temp->rptr;
        }
    }
    else
    {
        throw "List is Empty\n";
    }

    return big->get_key();
}

int List::get_sum() const
{
    Node* temp = head;
    int sum = 0;

    if (temp)
    {
        while (temp)
        {
            sum += temp->get_key();
            temp = temp->rptr;
        }
    }
    else
    {
        throw "List is Empty\n";
    }

    return sum;
}

void List::disp_squares() const
{
    Node* temp = head;

    if (temp)
    {
        while (temp)
        {
            cout << (temp->get_key() * temp->get_key()) << ", ";
            temp = temp->rptr;
        }
        cout << "\b\b   \n";
    }
    else
    {
        throw "List is Empty\n";
    }
}

void List::disp_sqrts() const
{
    Node* temp = head;
    int val;
    if (temp)
    {
        while (temp)
        {
            val = temp->get_key();
            if (val < 0)
                cout << sqrt(-val) << "i, ";
            else
                cout << sqrt(val) << ", ";
            temp = temp->rptr;
        }
        cout << "\b\b   \n";
    }
    else
    {
        throw "List is Empty\n";
    }
}
