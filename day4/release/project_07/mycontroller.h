#ifndef MYCONTROLLER_H
#define MYCONTROLLER_H

#include"controller.h"
#include "model.h"

// MyController : defn inherits Controller class
class MyController : public Controller
{
public:
	MyController(Model *pModel) : Controller(pModel) { pModel->setController(this); }
};

#endif
