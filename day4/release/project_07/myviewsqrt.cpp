#include <iostream>
#include "myviewsqrt.h"
#include"mymodel.h"

void MyViewSqrt::print_sqrts() const
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	List t = p->getdata();
	
	try
	{
		t.disp_sqrts();
	}
	catch(const char *s)
	{
		std::cout << s;
	}

}

