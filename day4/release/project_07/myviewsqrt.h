#ifndef MYVIEWSQRT_H
#define MYVIEWSQRT_H
#include "view.h"

class MyViewSqrt : public View
{
public:
	virtual void print_sqrts() const;
};

#endif
