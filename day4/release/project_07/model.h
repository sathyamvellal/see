#ifndef MODEL_H
#define MODEL_H
#include"controller.h"

class Controller;

// Model : definition
class Model
{
public:
	virtual ~Model() { }
	virtual void change();
	virtual void setController(Controller *p);
private:
	Controller *pController;
};


#endif
