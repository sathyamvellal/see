#include"myview.h"
#include"mymodel.h"
#include<cmath>

using namespace std;


void MyView::disp_forward() const
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	List t = p->getdata();
	t.disp_forward();
}

void MyView::disp_reverse() const
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	List t = p->getdata();
	t.disp_reverse();
}



