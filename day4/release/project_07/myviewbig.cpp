#include <iostream>
#include "myviewbig.h"
#include"mymodel.h"

void MyViewBig::biggest() const
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	List t = p->getdata();
	
	try
	{
		int num = t.get_biggest();
		std::cout << "The biggest number is: " << num;
	}
	catch(const char *s)
	{
		std::cout << s;
	}

}
