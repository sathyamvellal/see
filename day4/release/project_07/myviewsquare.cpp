#include <iostream>
#include "myviewsquare.h"
#include "mymodel.h"

void MyViewSquare::print_squares() const
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	List t = p->getdata();
	
	try
	{
		t.disp_squares();
	}
	catch(const char *s)
	{
		std::cout << s;
	}
}
