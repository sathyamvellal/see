#include"controller.h"

// controller : implementation
void Controller::attach(View *pView)		// A new viewer is added
{
	pViews[n++] = pView;

}

void Controller::detach(View *pView)		// Viewer now is not under the control of controller i.e any changes in model will not be shown up
{
	for(int i=0;i<n;i++)
	{
		if(pViews[i]==pView)
		{
			pViews[i]=0;
			i=n;
		}
	}
}

Model* Controller::getModel()
{
	return pModel;
}


