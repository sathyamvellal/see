#ifndef MYVIEWSQUARE_H
#define MYVIEWSQUARE_H
#include "view.h"

class MyViewSquare : public View
{
public:
	virtual void print_squares() const;
};


#endif
