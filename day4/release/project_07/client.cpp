#include<iostream>
#include"mycontroller.h"
#include"mymodel.h"
#include"myview.h"
#include"myviewsum.h"
#include"myviewbig.h"
#include"myviewsquare.h"
#include"myviewsqrt.h"

using namespace std;

int main()
{
	MyModel model;				
	MyController ctrl(&model);
	
	char choice1 , choice2;
	MyView view;
	MyViewSum viewsum;
	MyViewBig viewbig;
	MyViewSquare viewsquare;
	MyViewSqrt viewsqrt;
	view.attach(&ctrl);
	viewsum.attach(&ctrl);
	viewbig.attach(&ctrl);
	viewsquare.attach(&ctrl);
	viewsqrt.attach(&ctrl);

	do
	{
		cout << "\nEnter the choice :\n";
		cout << "1 : Edit model\n2 : Display View\n3 : Find Biggest\n4 : Find Sum\n5 : Find Squares\n6 : Find Square roots\n7 : Exit" << endl;
		cin >> choice1;
		switch(choice1)
		{
		case '1' :
			model.change();
			break;
		case '2' :
			cout << "\nEnter the choice :\n";
			cout << "1 : Forward display\n2 : Reverse Display\n";
			cin >> choice2;
			if(choice2 == '1')
			{
				view.disp_forward();
			}
			else
			{
				view.disp_reverse();
			}
			break;
		case '3' :
			viewbig.biggest();
			break;
		case '4' :
			viewsum.sum();
			break;
		case '5' :
			viewsquare.print_squares();
			break;
		case '6' :
			viewsqrt.print_sqrts();
			break;
		default : break;
		}
	}while(choice1 != '7');

	return 0;
}
