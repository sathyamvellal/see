#ifndef MYVIEWSUM_H
#define MYVIEWSUM_H
#include "view.h"

class MyViewSum : public View
{
public:
	virtual void sum() const;
};

#endif
