#include <iostream>
#include "myviewsum.h"
#include"mymodel.h"

void MyViewSum::sum() const
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	List t = p->getdata();
	try
	{
		int num = t.get_sum();
		std::cout << "The sum of numbers is: " << num;
	}
	catch(const char *s)
	{
		std::cout << s;
	}
}
