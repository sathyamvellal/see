#ifndef MYMODEL_H
#define MYMODEL_H
#include"model.h"

using namespace std;

class Node
{
	public :
		Node* lptr;
		Node* rptr;
		Node(int n) {key = n;}
        int get_key() {return key;}
	private :
		int key;
};

class List
{
	private :
		Node* head;
		Node* tail;
	public :
        List(){head = 0; tail = 0;}
        void add_at_beginning(int);
        void add_at_end(int);
        void remove_at_beginninig();
        void remove_at_end();
        void disp_forward();
        void disp_reverse();
        int get_biggest() const;
        int get_sum() const;
        void disp_squares() const;
        void disp_sqrts() const;
        ~List() {}
};


class MyModel : public Model
{
public:
	MyModel(){}
	virtual void change();
	virtual List getdata();

private:
	List list;
};

#endif
