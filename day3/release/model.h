/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2012
 */

#ifndef MODEL_H
#define MODEL_H
#include "controller.h"

//Forward Declaration
class Controller;

class Model
{
public:
	//Constructor
	Model() : pController(0) {}

	//Destructor
	virtual ~Model() {}

	//notifies the controller that the model has changed
	//can throw an exception if no controller is present
	virtual void change();

	//sets the member pController
	//Assumption that the client does not call this.
	virtual void setController(Controller *p);

private:
	Controller *pController;
};

#endif
