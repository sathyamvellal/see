/**
 * @authors Sathyam Vellal, Sidhi Adkoli
 * Created on: 09-01-2013
 */

#include <iostream>
#include "myviewreverse.h"
#include "node.h"
#include "mymodel.h"

void MyViewReverse::disp()
{
	std::cout << "Reverse traversal: \n";
	const Node& temp = cur;

	if (temp)
	{
		while (temp)
		{
			std::cout << temp->getVal() << " -> ";
			temp = temp->getPrev();
		}
		std::cout << "\b\b\b    \n";
	}
	else
	{
		std::cout << "NULL\n";
	}
}

void MyViewReverse::update()
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	cur = p->getTail();
}
