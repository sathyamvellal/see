/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2013
 * Last modified on: 09-01-2013
 */

#ifndef MYMODEL_H
#define MYMODEL_H
#include "model.h"
#include "list.h"

class Node;

//inherits Model
class MyModel : public Model
{
public:
	//constructor
	MyModel() {}
	
	//notifies that the model has changed
	virtual void change();
	
	//wrapper functions for adding and removing at end and beginning
	void addEnd(int num);
	int removeEnd();
	void addBeg(int num);
	int removeBeg();
	
	//returns head and tail of DList
	const Node& getHead();
	const Node& getTail();
	
private:
	DList d;		//doubly linked list
};

#endif
