/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2012
 * Last Modifed : 08-01-2012
 */

#include <iostream>
#include "mymodel.h"
#include "mycontroller.h"
#include "myviewforward.h"
#include "myviewreverse.h"

void menu()
{
	std::cout << "1. Add at the end\n"
			  << "2. Add at the beginning\n"
			  << "3. Remove at the end\n"
			  << "4. Remove at the beginning\n"
			  << "5. Display forward\n"
			  << "6. Display reverse\n"
			  << "7. Exit\n\n";
}

int main()
{
	MyModel m;
	MyController c(&m);

	MyViewForward vf;
	vf.attach(&c);
	MyViewReverse vr;
	vr.attach(&c);

	char choice;
	int val;

	menu();
	do
	{
		std::cout << "Enter Choice : ";
		std::cin >> choice;

		switch (choice)
		{
		case '1':
			std::cout << "Enter a value : ";
			std::cin >> val;
			m.addEnd(val);
		break;
		case '2':
			std::cout << "Enter a value : ";
			std::cin >> val;
			m.addBeg(val);
		break;
		case '3':
			try
			{		
				std::cout << "Removed : " << m.removeEnd() << "\n";
			}
			catch (const char* s)
			{
				std::cout << s << "\n";
			}
		break;
		case '4':
			try
			{		
				std::cout << "Removed : " << m.removeBeg() << "\n";
			}
			catch (const char* s)
			{
				std::cout << s << "\n";
			}
		break;
		case '5':
			vf.disp();
		break;
		case '6':
			vr.disp();
		break;
		case '7':
		break;
		default:
			std::cout << "Invalid choice!\n";
			menu();
		break;
		}

		std::cout <<"\n";
	} while (choice != '7');

	return 0;
}
