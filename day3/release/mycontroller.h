/**
 * @authors Sathyam M Vellal, Sidhi Adkoli
 * Created on : 07-01-2012
 */

#ifndef MYCONTROLLER_H
#define MYCONTROLLER_H
#include "controller.h"
#include "model.h"

//MyController inherits Controller
class MyController : public Controller
{
public:
	//constructor. Binds the controller to pModel
	MyController(Model *pModel);
};

#endif
