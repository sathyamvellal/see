/**
 * @authors Sathyam Vellal, Sidhi Adkoli
 * Created on: 09-01-13
 */


#ifndef MYVIEWREVERSE_H
#define MYVIEWREVERSE_H
#include "view.h"

class Node;

//MyViewReverse inherits from View
class MyViewReverse : public View
{
public:
	//displays the view
	virtual void disp();
	
	//Denotes that the view must be updated. Is called when the
	//	underlying model changes
	void update();
	
private:
	//used for traversing
	const Node& cur;
};

#endif

