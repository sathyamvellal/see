
#include <iostream>
using namespace std;
#include <cmath>

class Controller;

// Model : definition
class Model
{
public:
	virtual ~Model() { }
	virtual void change();
	virtual void setController(Controller *p);
private:
	Controller *pController;
};

// View : definition
class View
{
public:
	View() : pController(0) {}
	virtual ~View(){};
	virtual void attach(Controller *);
	virtual void detach();
	virtual void disp() = 0;
	virtual void update() = 0;

protected:
	Controller *pController;
};


// Controller : Definition
class Controller
{
public:
	Controller(Model *pModel = 0) : pModel(pModel), n(0) {}
	virtual void attach(View *pView);
	virtual void detach(View *pView); //not implemented
	virtual void notify();
	virtual Model* getModel();
	virtual ~Controller() { }
protected:
	Model *pModel;
	View  *pViews[10];
	int n;


};

// Model : Implementation
void Model::change()
{
	pController->notify();
}

void Model::setController(Controller *p)
		{
			pController = p;
		}

// View : Implementation
void View::attach(Controller *pCtrl)
{
	pController = pCtrl;
	pCtrl->attach(this);
	update();
}

void View::detach()
{
	pController->detach(this);

}

// controller : implementation
void Controller::attach(View *pView)
{
	pViews[n++] = pView;

}

void Controller::detach(View *pView)
{
//simplified
//	pView = 0;
}

Model* Controller::getModel()
{
	return pModel;
}

void Controller::notify()
{
	for(int i = 0; i < n; ++i)
		pViews[i]->update();
}



// Mymodel : defn
class MyModel : public Model
{
public:
	MyModel(int val = 0) : val(val) { }
	virtual void change();
	virtual int getdata();
private:
	int val;
};

// MyView : defn
class MyView1 : public View
{
public:
	virtual void disp();
	void update();
private:
	int sq;
};






// MyController : defn
class MyController : public Controller
{
public:
	MyController(Model *pModel) : Controller(pModel) { pModel->setController(this); }
};

// MyModel implementation
int MyModel::getdata()
	{
		return val;
	}
void MyModel::change()
{
	cout << "enter new value : ";
	cin >> val;
	Model::change();
}


// MyView : Implementation
void MyView1::update()
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	int t = p->getdata();
	sq = t * t;
}

void MyView1::disp()
{
cout << "Square : " << sq << endl;
}




int main()
{
	MyModel model(10);
	MyController ctrl(&model);

	MyView1 v1;
	v1.attach(&ctrl);


	v1.disp();

	model.change();

	v1.disp();

	return 0;
}
