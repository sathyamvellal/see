#include<iostream>
#include "model.h"

void Model::change()
{
	if (pController == 0)
		throw "No controller attached";
	pController->notify();
}

void Model::setController(Controller *p)
{
	if (pController != 0)
		throw "Illegal controller reassignment attempted!";
	pController = p;
}
