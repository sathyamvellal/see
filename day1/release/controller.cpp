#include"controller.h"

Controller::Controller(Model *pModel): pModel(pModel), nextAvailable(0)
{
	for (int i = 0; i < MAX; ++i)
		pViews[i] = 0;
}

void Controller::attach(View *pView)
{
	int i;
	if (nextAvailable == -1)
		throw "Failed to attach view!";
	pViews[nextAvailable] = pView;

	for (i = nextAvailable; i < MAX && pViews[i] != 0; i = (i + 1) % MAX);
	nextAvailable = i < MAX ? i : -1;
}

void Controller::detach(View *pView)
{
	int i;
	for (i = 0; i < MAX && pViews[i] != pView; ++i);
	if (i < MAX)
	{
		pViews[i] = 0;
	}
}

Model* Controller::getModel()
{
	return pModel;
}

void Controller::notify()
{
	for(int i = 0; i < MAX; ++i)
	{
		if (pViews[i] != 0)
		{
			pViews[i]->update();
		}
	}
}

