#include <iostream>
#include "mymodel.h"
#include "mycontroller.h"
#include "myview1.h"

int main()
{
	MyModel m(5);
	
	MyController c(&m);

	MyView1 v;
	v.attach(&c);

	v.disp();

	m.change();

	v.disp();

	v.detach();
	return 0;
}
