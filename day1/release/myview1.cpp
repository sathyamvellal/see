#include <iostream>
#include "myview1.h"
#include "mymodel.h"

void MyView1::update()
{
	MyModel *p = dynamic_cast <MyModel*> (pController->getModel());
	int t = p->getdata();
	sq = t * t;
}

void MyView1::disp()
{
	std::cout << "Square : " << sq << std::endl;
}
