#ifndef MYCONTROLLER_H
#define MYCONTROLLER_H
#include "controller.h"
#include "model.h"

//MyController inherits Controller
class MyController : public Controller
{
public:
	//constructor. Binds the controller to pModel
	MyController(Model *pModel);
};

#endif
