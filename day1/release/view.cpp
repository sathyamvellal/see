#include <iostream>
#include"view.h"

void View::attach(Controller *pCtrl)
{
	try
	{
		if (pController != 0)
			throw "Already attached to a controller. Detach first!";
		pController = pCtrl;
		pCtrl->attach(this);
		update();
	}
	catch (const char *s)
	{
		std::cout << s << std::endl;
	}
}

void View::detach()
{
	if (pController != 0)
	{
		pController->detach(this);
		pController = 0;
	}
}
