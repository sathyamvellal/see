#ifndef MYVIEW_H
#define MYVIEW_H
#include"view.h"

//MyView1 inherits from View
class MyView1 : public View
{
public:
	//displays the view
	virtual void disp();
	
	//Denotes that the view must be updated. Is called when the
	//	underlying model changes
	void update();
private:
	int sq;
};

#endif
