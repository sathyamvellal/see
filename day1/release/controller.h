#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "model.h"
#include "view.h"
#define MAX 10

//forward declaration for Model and View classes
class Model;
class View;


class Controller
{
public:
	//constructor. Binds the class to model object 
	Controller(Model *pModel);
	
	//attaches pView to the controller
	//throws exception if the views list is full
	virtual void attach(View *pView);
	
	//detaches pView from the controller
	virtual void detach(View *pView);
	
	//notifies the view that the model has changed
	virtual void notify();
	
	//returns the model to which it is bound
	virtual Model* getModel();
	
	//destructor
	virtual ~Controller() { }
protected:
	Model *pModel;
	
	//stores an array of pointers to views
	View  *pViews[MAX];
		
	//stores the location of the next available space to store a view
	int nextAvailable;
};

#endif
